import React from "react";
import { Layout } from "antd";

const { Footer: AntFooter } = Layout;

function Footer(){
  return (
      <>
        <AntFooter>
          <p>2021 Ironhorse Studios</p>
        </AntFooter>
      </>
  )
}

export default Footer;