import StockResult from "./StockResult";
import {Col, Row} from 'antd';
import './card.css';

function StockInfoList({stockList = []}) {
  return (
      <>
          {stockList.map((data, index) => {
            if (data) {
              return (
                  <Row gutter={{xs: 8, sm: 16, md: 24, lg: 32}}>
                    <Col className="gutter-box" span={24}>
                      <StockResult stock={data} idx={index} className="card"/>
                    </Col>
                  </Row>
              );
            }
            return null;
          })}
      </>
  )
}

export default StockInfoList;