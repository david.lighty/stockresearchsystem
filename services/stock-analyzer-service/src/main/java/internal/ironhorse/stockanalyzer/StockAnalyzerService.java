package internal.ironhorse.stockanalyzer;

import internal.ironhorse.stockanalyzer.controller.DefaultController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockAnalyzerService {
    public static void main(String[] args) {
        SpringApplication.run(DefaultController.class, args);
    }
}
