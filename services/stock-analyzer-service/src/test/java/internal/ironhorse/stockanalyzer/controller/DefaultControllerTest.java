package internal.ironhorse.stockanalyzer.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DefaultControllerTest {

    private DefaultController testController;

    @BeforeEach
    public void setup(){
        testController = new DefaultController();
    }

    @Test
    public void testGetInfo(){
        var resp = testController.getInfo();
        Assertions.assertTrue(resp.getStatusCode() == HttpStatus.OK);
    }
}