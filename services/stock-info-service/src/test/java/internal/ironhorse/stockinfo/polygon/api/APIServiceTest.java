package internal.ironhorse.stockinfo.polygon.api;

import internal.ironhorse.http.Http;
import internal.ironhorse.stockinfo.config.APIConfig;
import internal.ironhorse.stockinfo.polygon.api.response.TickersV3Response;
import internal.ironhorse.stockinfo.polygon.util.JsonUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.net.ssl.SSLSession;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class APIServiceTest {
    @Mock
    private Http mockHttp;

    @Mock
    private APIConfig mockConfig;

    private APIService testSvc;
    private AutoCloseable mocked;

    @BeforeEach
    public void setupTest() {
        mocked = MockitoAnnotations.openMocks(this);
        testSvc = new APIService(mockConfig, mockHttp);
    }

    @AfterEach
    public void cleanup() throws Exception {
        mocked.close();
    }

    @Test
    public void testGetStocks() throws Exception {
        String expectedEndpoint = "/" + Endpoints.TICKERS.getEndpoint() + "?search=MSFT&apiKey=1";
        HttpResponse<String> mockResponse = new HttpResponse<String>() {
            @Override
            public int statusCode() {
                return 200;
            }

            @Override
            public HttpRequest request() {
                return null;
            }

            @Override
            public Optional<HttpResponse<String>> previousResponse() {
                return Optional.empty();
            }

            @Override
            public HttpHeaders headers() {
                return null;
            }

            @Override
            public String body() {
                return "{\n" +
                        " \"page\": 1,\n" +
                        " \"perPage\": 50,\n" +
                        " \"count\": 2,\n" +
                        " \"status\": \"OK\",\n" +
                        " \"tickers\": [\n" +
                        "  {\n" +
                        "   \"ticker\": \"MSF\",\n" +
                        "   \"name\": \"Microsoft Corporation\",\n" +
                        "   \"market\": \"STOCKS\",\n" +
                        "   \"locale\": \"US\",\n" +
                        "   \"currency\": \"CHF\",\n" +
                        "   \"active\": true,\n" +
                        "   \"primaryExch\": \"NYE\",\n" +
                        "   \"updated\": \"2020-06-23\",\n" +
                        "   \"codes\": {\n" +
                        "    \"figiuid\": \"EQ0000000001846988\",\n" +
                        "    \"scfigi\": \"BBG001S5TD05\",\n" +
                        "    \"cfigi\": \"BBG000LF7Z07\",\n" +
                        "    \"figi\": \"BBG006M6ZMY4\"\n" +
                        "   },\n" +
                        "   \"url\": \"https://api.polygon.io/v2/tickers/MSF\"\n" +
                        "  },\n" +
                        "  {\n" +
                        "   \"ticker\": \"MSFT\",\n" +
                        "   \"name\": \"Microsoft Corporation Common Stock\",\n" +
                        "   \"market\": \"STOCKS\",\n" +
                        "   \"locale\": \"US\",\n" +
                        "   \"currency\": \"USD\",\n" +
                        "   \"active\": true,\n" +
                        "   \"primaryExch\": \"NASDAQ\",\n" +
                        "   \"updated\": \"2020-06-23\",\n" +
                        "   \"codes\": {\n" +
                        "    \"cik\": \"0000789019\",\n" +
                        "    \"figiuid\": \"EQ0010174300001000\",\n" +
                        "    \"scfigi\": \"BBG001S5TD05\",\n" +
                        "    \"cfigi\": \"BBG000BPH459\",\n" +
                        "    \"figi\": \"BBG000BPHFS9\"\n" +
                        "   },\n" +
                        "   \"url\": \"https://api.polygon.io/v2/tickers/MSFT\"\n" +
                        "  }\n" +
                        " ]\n" +
                        "}";
            }

            @Override
            public Optional<SSLSession> sslSession() {
                return Optional.empty();
            }

            @Override
            public URI uri() {
                return null;
            }

            @Override
            public HttpClient.Version version() {
                return null;
            }
        };
        TickersV3Response expectedResponse = JsonUtil.fromJson(mockResponse.body(), TickersV3Response.class);
        Mockito.when(mockHttp.GET(any()))
                .thenReturn(mockResponse);
        Mockito.when(mockConfig.constructAPIEndpoint()).thenReturn("test");
        Mockito.when(mockConfig.getKey()).thenReturn("1");

        TickersV3Response tickerResponse = testSvc.performSearch("MSFT");

        Assertions.assertEquals(expectedResponse.toString(), tickerResponse.toString());
        verify(mockHttp, times(1)).GET(eq(expectedEndpoint));
        verify(mockConfig, times(1)).constructAPIEndpoint();
        verify(mockConfig, times(1)).getKey();
    }
}