package internal.ironhorse.stockinfo.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class StockInfoControllerTest {

    @Autowired
    StockInfoController clientController;

    @Test
    public void testApiClientLayerExists(){
        Assertions.assertNotNull(clientController);
    }
}