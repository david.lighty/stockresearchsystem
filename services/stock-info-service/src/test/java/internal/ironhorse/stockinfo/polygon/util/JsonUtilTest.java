package internal.ironhorse.stockinfo.polygon.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

class JsonUtilTest {

    @Test
    public void testCanSerialize() throws JsonProcessingException {
        Map<String, String> itemMap = Map.of(
                "value1", "1",
                "value2", "2"
        );

        String json = JsonUtil.toJson(itemMap);

        Assertions.assertTrue(json.contains("value1"));
    }

    @Test
    public void testCanDeSerialize(){
        String json = "{\"value1\":\"1\",\"value2\":\"2\"}";
        Map<String, String> expectedMap = Map.of(
                "value1", "1",
                "value2", "2"
        );


    }
}