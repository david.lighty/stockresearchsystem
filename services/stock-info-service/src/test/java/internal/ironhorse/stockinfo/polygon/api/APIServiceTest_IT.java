package internal.ironhorse.stockinfo.polygon.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("polygon")
@Disabled
class APIServiceTest_IT {

    @Autowired
    private APIService testSvc;


    @Test
    public void testSearchStocks() throws Exception {
        var searchResponse = testSvc.performSearch("Apple");
        Assertions.assertNotNull(searchResponse);
    }

    @Test
    public void testGetCompany() throws Exception {
        var companyResponse = testSvc.getCompany("AAPL");
        Assertions.assertNotNull(companyResponse);
    }

    @Test
    public void testGetExchanges() throws Exception {
        var response = testSvc.getExchanges();
        Assertions.assertNotNull(response);
    }
}