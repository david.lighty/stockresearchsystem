package internal.ironhorse.stockinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import internal.ironhorse.exceptions.RestException;
import internal.ironhorse.stockinfo.service.APIClientService;

/**
 * Entry point for all APIClientLayer calls.
 */
@RestController
@CrossOrigin
public class ExchangeController {
    private final APIClientService clientLayer;

    @Autowired
    public ExchangeController(APIClientService clientLayer) {
        this.clientLayer = clientLayer;
    }

//    @GetMapping(path = "/markets/{name}")
//    public ResponseEntity<?> getMarket(
//            @PathVariable String name) throws Exception {
//        List<Stock> searched = this.clientLayer.getMarket(name);
//        return ResponseEntity.ok(searched);
//    }

    @GetMapping(path="/exchanges")
    public ResponseEntity<?> getExchanges() throws RestException {
        return ResponseEntity.ok(this.clientLayer.getExchanges());
    }

}
