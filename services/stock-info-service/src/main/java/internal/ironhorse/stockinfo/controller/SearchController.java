package internal.ironhorse.stockinfo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import internal.ironhorse.library.api.Stock;
import internal.ironhorse.stockinfo.service.APIClientService;

/**
 * Entry point for all APIClientLayer calls.
 */
@RestController
@CrossOrigin
public class SearchController {
    private final APIClientService clientLayer;

    @Autowired
    public SearchController(APIClientService clientLayer) {
        this.clientLayer = clientLayer;
    }

    @GetMapping(path = "/info/search/{name}")
    public ResponseEntity<?> searchForStock(
            @PathVariable String name) throws Exception {
        List<Stock> searched = this.clientLayer.searchStockInfo(name);
        return ResponseEntity.ok(searched);
    }

}
