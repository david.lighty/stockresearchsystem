package internal.ironhorse.stockinfo.polygon.api;

import lombok.Data;

@Data
public class Exchange {
     public String acronym;
     public String asset_class;
     public int id;
     public String locale;
     public String mic;
     public String name;
     public String operating_mic;
     public String participant_id;
     public String type;
     public String url;


     public String getLocale(){
          return locale.toUpperCase();
     }
}
