package internal.ironhorse.stockinfo.polygon.api.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import internal.ironhorse.library.api.Exchange;
import internal.ironhorse.library.api.Stock;
import internal.ironhorse.stockinfo.polygon.api.Ticker;
import internal.ironhorse.stockinfo.polygon.api.response.CompanyV1Response;


@Mapper(componentModel = "spring")
public interface APIMapper {

//  APIMapper INSTANCE = Mappers.getMapper(APIMapper.class);

  // Company -> Stock
  Stock companyToStock(CompanyV1Response companyV1Response);

  @Mapping(source = "ticker", target = "symbol")
  @Mapping(source = "primary_exchange", target = "exchange")
  @Mapping(source = "last_updated_utc", target = "updated")
  Stock tickerToStock(Ticker tickersV1Response);

  @Mapping(source = "locale", target = "locale")
  Exchange exchangeToExchange(internal.ironhorse.stockinfo.polygon.api.Exchange resp);
}
