package internal.ironhorse.stockinfo.service;

import java.util.List;

import internal.ironhorse.exceptions.RestException;
import internal.ironhorse.library.api.Exchange;
import internal.ironhorse.library.api.Stock;
import internal.ironhorse.stockinfo.config.APIConfig;

public class APICacheLayer implements APIClientService {

  private final APIConfig config;
  private final APIClientLayer clientLayer;
  //memcache

  public APICacheLayer(APIConfig config,
      APIClientLayer apiClientLayer) {
    this.config = config;
    this.clientLayer = apiClientLayer;
  }

  @Override
  public List<Stock> searchStockInfo(String testTickerOrName) throws Exception {
    return clientLayer.searchStockInfo(testTickerOrName);
  }

  @Override
  public Stock getStockInfo(String stockTicker) throws RestException {
    return clientLayer.getStockInfo(stockTicker);
  }

  @Override
  public void getStockNews(String stockTicker) {
    clientLayer.getStockNews(stockTicker);
  }

  @Override
  public Exchange getExchange(String name) {
    return null;
  }

  @Override
  public List<Exchange> getExchanges() throws RestException {
    return clientLayer.getExchanges();
  }
}
