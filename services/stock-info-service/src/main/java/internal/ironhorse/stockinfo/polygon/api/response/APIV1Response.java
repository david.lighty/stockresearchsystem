package internal.ironhorse.stockinfo.polygon.api.response;

abstract class APIV1Response extends APIResponse {
    private String status;
    private String requestId;

    @Override
    public String toString() {
        return "APIV1Response{" +
                "status='" + status + '\'' +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
