package internal.ironhorse.stockinfo.polygon.api;

import lombok.Data;

@Data
public class Ticker {
    private String ticker;
    private String market;
    private String locale;
    private String name;
    private String primary_exchange;
    private String currency_name;
    private Boolean active;
    private String last_updated_utc;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
//    private LocalDate last_updated_utc;
}
