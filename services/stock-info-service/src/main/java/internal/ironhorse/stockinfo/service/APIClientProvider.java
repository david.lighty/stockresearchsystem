package internal.ironhorse.stockinfo.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import internal.ironhorse.http.Http;
import internal.ironhorse.stockinfo.config.APIConfig;
import internal.ironhorse.stockinfo.polygon.api.mappers.APIMapper;

/**
 * Create all needed layers and connect them.
 */
@Configuration
public class APIClientProvider {

  private final APIConfig config;
  private final Http http;
  private final APIMapper mapper;
  private final EventHandlerService eventHandlerService;

  public APIClientProvider(APIConfig config, Http http,
      APIMapper mapper, EventHandlerService eventHandlerService) {
    this.config = config;
    this.http = http;
    this.mapper = mapper;
    this.eventHandlerService = eventHandlerService;
  }

  @Bean
  public APIClientService createLayers() {
    PolygonClientLayer polygonClientLayer = new PolygonClientLayer(this.config, this.http,
        this.mapper);
    return new APICacheLayer(this.config,
        new APIClientLayer(this.config, polygonClientLayer, eventHandlerService));
  }
}
