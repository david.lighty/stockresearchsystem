package internal.ironhorse.stockinfo.polygon.api.response;

import lombok.Data;

/**
 * Full company response
 */
@Data
public class CompanyV1Response extends APIResponse {
    private String logo;
    private String listdate;
    private String cik;
    private String bloomberg;
    private String figi = null;
    private String lei;
    private Float sic;
    private String country;
    private String industry;
    private String sector;
    private Float marketcap;
    private Float employees;
    private String phone;
    private String ceo;
    private String url;
    private String description;
    private String exchange;
    private String name;
    private String symbol;
    private String exchangeSymbol;
    private String hq_address;
    private String hq_state;
    private String hq_country;
    private String type;
    private String updated;
    //    List < Object > tags = new ArrayList < Object > ();
//    List<String> similar = new ArrayList<String>();
    private Boolean active;

}
