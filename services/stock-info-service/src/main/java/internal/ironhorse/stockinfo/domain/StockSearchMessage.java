package internal.ironhorse.stockinfo.domain;

import java.util.List;

public class StockSearchMessage extends EventMessage {

  private String searchTerm;
  private Boolean successful;
  private List<String> stockSymbols;

  public StockSearchMessage(String searchTerm, Boolean successful, List<String> stockSymbols) {
    this.searchTerm = searchTerm;
    this.successful = successful;
    this.stockSymbols = stockSymbols;
  }
}
