package internal.ironhorse.stockinfo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.NotImplementedException;

import internal.ironhorse.exceptions.RestException;
import internal.ironhorse.library.api.Exchange;
import internal.ironhorse.library.api.Stock;
import internal.ironhorse.stockinfo.config.APIConfig;
import internal.ironhorse.stockinfo.domain.StockSearchMessage;
import internal.ironhorse.stockinfo.domain.StockTickerMessage;
import internal.ironhorse.topics.Topic;

/**
 * Main "Client" to be consumed and service outside calls. Cache and other layers are to service
 * this layer.
 */
public class APIClientLayer implements APIClientService {

  private final PolygonClientLayer polygonClientLayer;
  private final APIConfig config;
  private final EventHandlerService evtHandlerSvc;

  public APIClientLayer(APIConfig config, PolygonClientLayer polygonClientLayer,
      EventHandlerService handlerService) {
    this.config = config;
    this.polygonClientLayer = polygonClientLayer;
    this.evtHandlerSvc = handlerService;
  }

  @Override
  public List<Stock> searchStockInfo(String searchTerm) throws Exception {
    List<Stock> results = this.polygonClientLayer.searchStockInfo(searchTerm);
    StockSearchMessage msg = new StockSearchMessage(
        searchTerm,
        !results.isEmpty(),
        results.stream()
            .map(Stock::getSymbol)
            .collect(Collectors.toList())
    );
    this.evtHandlerSvc.sendMessage(Topic.STOCK_SEARCH, searchTerm, msg);
    return results;
  }

  @Override
  public Stock getStockInfo(String stockTicker) throws RestException {
    Stock stockInfo = this.polygonClientLayer.getStockInfo(stockTicker);
    StockTickerMessage msg = new StockTickerMessage(
        stockInfo == null ? "no result" :
        stockInfo.getSymbol()
    );
    this.evtHandlerSvc.sendMessage(Topic.STOCK_TICKER, stockTicker, msg);
    return stockInfo;
  }

  @Override
  public void getStockNews(String stockTicker) {
    throw new NotImplementedException("Not built yet");
  }

  @Override
  public Exchange getExchange(String name) {
    return null;
  }

  @Override
  public List<Exchange> getExchanges() throws RestException {
    List<Exchange> results = this.polygonClientLayer.getExchanges();
    // send event
    return results;
  }
}
