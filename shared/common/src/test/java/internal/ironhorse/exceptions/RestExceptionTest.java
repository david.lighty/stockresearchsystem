package internal.ironhorse.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RestExceptionTest {

    @Test
    public void throwsRestException() {
        Assertions.assertThrows(RestException.class, () -> {
            throw new RestException("Rest Exception", new Exception());
        });
    }
}