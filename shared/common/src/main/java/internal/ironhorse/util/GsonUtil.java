package internal.ironhorse.util;

import java.lang.reflect.Type;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Injectable Util class to serialize and de-serialize JSON
 * using GSON
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class GsonUtil {
  Gson parser;

  public GsonUtil(){
    parser = new GsonBuilder().create();
  }

  public <T> T parse(String json, Type clazz){
    return this.parser.fromJson(json, clazz);
  }

  public <T> String toJson(T obj){
    return this.parser.toJson(obj);
  }
}
