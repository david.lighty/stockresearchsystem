package internal.ironhorse.topics;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.kafka.config.TopicBuilder;

public class DefaultTopicFactory {

  public static NewTopic getBasicTopic(String name, int partitionCount, int replicaCount) {
    return TopicBuilder
        .name(name)
        .partitions(partitionCount)
        .replicas(replicaCount)
        .build();
  }
}
