package internal.ironhorse.http;

import internal.ironhorse.exceptions.RestException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class Http {
    private final HttpClient httpClient;
    private String baseURL;

    public Http(){
        httpClient = HttpClient.newHttpClient();
    }

    public HttpResponse<String> GET(String endpoint) throws RestException {
        HttpRequest request = createRequest(endpoint)
                .GET()
                .build();
        return send(request);
    }

//    public HttpResponse<String> POST(String url, String body) throws RestException {
//        return send(createRequest(url)
//                .POST(HttpRequest.BodyPublishers.ofString(body))
//                .build());
//    }
//
//    public HttpResponse<String> PATCH(String url, String body) throws RestException {
//        return send(createRequest(url)
//                .PUT(HttpRequest.BodyPublishers.ofString(body))
//                .build());
//    }
//
//    public void DELETE(String url) throws RestException {
//        send(createRequest(url).DELETE().build());
//    }

    public void setBaseURL(String baseURL){
        this.baseURL = baseURL;
    }

    /**
     * Main HTTP handler, catch exceptions and re-throw as our
     * own internal.ironhorse.exceptions.RestException.
     */
    private HttpResponse<String> send(HttpRequest request) throws RestException{
        try {
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            // TODO - LOG
            throw new RestException("Error with rest client", e);
        }
    }

    private HttpRequest.Builder createRequest(String endpoint) throws RestException {
        if(!endpoint.startsWith("/") && !endpoint.startsWith("?")){
            throw new RestException("Endpoint missing required starting character (/:?)");
        }
        return HttpRequest
                .newBuilder(URI.create(baseURL + endpoint))
                // TODO - add auth map
                // TODO - add headers
                ;
    }
}
