package internal.ironhorse.library.api;

import lombok.*;

/**
 * Stock
 * Holds basic info about a stock for our services.
 * Should be used to link into other areas as needed.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Stock {
//    private String logo;
//    private String listdate;
//    private String cik;
//    private String bloomberg;
//    private String figi = null;
//    private String lei;
//    private Float sic;
    private String country;
    private String industry;
    private String sector;
    private Float marketcap;
//    private float employees;
//    private String phone;
    private String ceo;
    private String url;
    private String description;
    private String exchange;
    private String name;
    private String symbol;
    private String exchangeSymbol;
//    private String hq_address;
//    private String hq_state;
//    private String hq_country;
//    private String type;
    private String updated;
//    private boolean active;
}
