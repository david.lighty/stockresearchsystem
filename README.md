# SpringKubeTest Project

Goal
----
Create a simple multi-service project.  Setup both k8 and non-k8 work
as seen in a Spring Developer demo project.  This will include basic
service discovery, dynamic config, etc.

Build
-----

This is a maven multi-module project, from the
root of the project run:

`mvn clean install compile -DskipTests`

To build all modules, or *add*:

`-pl <MODULE NAME> -am`

to build just one, example:

`mvn clean install compile -DskipTests -pl shared/common -am`

To make this all easier, there is the project.sh bash script:

`./scripts/project.sh -b all`
Optional: `-b <MODULE NAME>`


Run
---
First build all the images for the modules:

`./scripts/project.sh -i all`

Then start infrastructure images:

`./scripts/project.sh -u`

To turn off:

`./scripts/project.sh -d`

Docker Compose
--------------
Start stock-info-service:

`docker-compose -f services-compose.yml up -d`

to scale: `--scale stockinfoservice=3`

The docker compose file is setup in a way to allow scaling with dynamic ports and
*no* custom container_name, which allows numbering.  (stock_1, stock_2, etc)

Dependencies
------------
List any major items here.
* Spring Boot
* Spring Actuator
* Spring Web
* Maven
* Netflix Eureka
* Lombok
* Mapstruct

**Additional Developer Goals**
* Learn GitLab
* Learn k8s
* Learn service discovery, short circuiting, etc.
* Re-setup a new project again, after working on a stale on for so long.